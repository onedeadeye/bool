using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [HideInInspector]
    public bool isDummy = false;
    [HideInInspector]
    public bool active = false;

    [SerializeField]
    private float gravity = 0f;

    [SerializeField]
    private bool bouncy = false;
    private float bounceDrag = 1f;
    // A projectile that tries to "bounce" while moving slower than this value will instead... do something. Maybe explode?
    private float bounceVelocityCutoff = 1f;

    [SerializeField]
    private float forwardVelocity = 0f;

    [SerializeField]
    private float startingUpVelocity = 0f;

    [SerializeField]
    private float lifetime = 0f;

    [SerializeField]
    private bool triggerOnDecay = false;

    public Team Team;

    [SerializeField]
    private GameObject impactPrefab;

    [SerializeField]
    private float impactDamage = 1f;

    // live variables
    private float upVelocity = 0f;

    // Start is called before the first frame update
    void Start()
    {
        upVelocity = startingUpVelocity;

        // To fix the jank of a full shotgun blast erupting from your nose, projectiles are hidden
        // until they are a certain distance from the camera.
        if (!isDummy)
        {
            StartCoroutine(ScheduleReveal());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!active)
        {
            return;
        }

        if (lifetime > 0f)
        {
            StartCoroutine(ScheduleDecay());
        }

        Vector3 move = new Vector3(0, 1, 1);
        move.z = move.z * forwardVelocity * Time.deltaTime;
        move.y = move.y * upVelocity * Time.deltaTime;

        transform.Translate(move, Space.Self);

        upVelocity -= gravity * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player") || col.CompareTag("Enemy"))
        {
            Team hitTeam = col.GetComponent<ICharacter>().GetTeam();
            if (hitTeam != Team && hitTeam != Team.NONE)
            {
                if (impactPrefab)
                {
                    TriggerImpact();
                }
                else
                {
                    if (!isDummy)
                    {
                        col.GetComponent<ICharacter>().TakeDamage(impactDamage);
                    }
                    Destroy(gameObject);
                }
            }
        }

        if (col.CompareTag("Environment"))
        {
            if (bouncy)
            {
                forwardVelocity -= bounceDrag;

                if (forwardVelocity < bounceVelocityCutoff)
                {
                    Decay();
                }

                RaycastHit contact;
                Physics.Raycast(transform.position, transform.forward, out contact, 10f);

                Vector3 newForward = Vector3.Reflect(transform.forward, contact.normal);

                transform.rotation = Quaternion.LookRotation(newForward);
            }
            else
            {
                TriggerImpact();
            }
        }
    }

    private void TriggerImpact()
    {
        if (impactPrefab)
        {
            GameObject newImpact = Instantiate(impactPrefab);
            ProjectileResult newResult = newImpact.GetComponent<ProjectileResult>();
            newImpact.transform.position = transform.position;
            newResult.isDummy = isDummy;
            newResult.team = Team;

            newResult.isSet = true;

            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    IEnumerator ScheduleReveal()
    {
        Transform graphics = transform.Find("Graphics");
        if (graphics)
        {
            transform.Find("Graphics").gameObject.SetActive(false);
            yield return new WaitForSeconds(1 / forwardVelocity);
            transform.Find("Graphics").gameObject.SetActive(true);
        }
        else
        {
            yield return new WaitForSeconds(0);
        }
    }

    IEnumerator ScheduleDecay()
    {
        yield return new WaitForSeconds(lifetime);
        Decay();
    }

    private void Decay()
    {
        if (triggerOnDecay)
        {
            TriggerImpact();
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
