using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiProjectile : Projectile
{
    [Header("Multi Projectile Settings")]
    [SerializeField]
    private GameObject projectile;
    [SerializeField]
    private int amount = 11;
    [SerializeField]
    private bool centerProjectile = true;
    [SerializeField]
    private float spreadMinimum = 5f;
    [SerializeField]
    private float spreadMaximum = 10f;

    void Start()
    {
        float individualRotation = 360f / (amount - ((centerProjectile) ? 1f : 0f));
        System.Random random = new System.Random();

        for (int i = 0; i < amount; i++)
        {
            GameObject newProjectileGO = Instantiate(projectile);
            Projectile newProjectile = newProjectileGO.GetComponent<Projectile>();

            newProjectileGO.transform.position = transform.position;
            newProjectileGO.transform.forward = transform.forward;

            // If we have a center, unadjusted projectile, skip this for the first projectile
            if (centerProjectile && i == 0)
            {
                goto InitProjectile;
            }

            float pitchOffset = (float)(random.NextDouble() * ((spreadMaximum - spreadMinimum)) + spreadMinimum);
            float rotationOffset = (i - (centerProjectile ? 1f : 0f)) * individualRotation;
            newProjectileGO.transform.Rotate(0, 0, rotationOffset, Space.Self);
            newProjectileGO.transform.Rotate(pitchOffset, 0, 0, Space.Self);

        InitProjectile:
            newProjectile.isDummy = isDummy;
            newProjectile.Team = Team;
            newProjectile.active = true;
        }

        Destroy(gameObject);
    }
}
