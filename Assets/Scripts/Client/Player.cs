using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class Player : NetworkBehaviour, ICharacter
{
    // ICharacter implementation
    public string Name { get; set; }
    public int MaxHealth { get; set; }
    public float CurrentHealth { get; set; }
    public bool IsDead { get; set; }
    public Team Team = Team.NONE;
    public StatusEffectList SEList { get; set; }

    [SerializeField]
    private int playerMaxHealth = 100;

    private bool HasGameStarted;

    private CharacterController controller;
    [SerializeField]
    private GameObject cameraGO;
    [SerializeField]
    private Text nameplateText;

    [SerializeField]
    private Transform groundCheck;
    private float groundDistance = 0.1f;
    [SerializeField]
    private LayerMask groundMask;

    [SerializeField]
    private float speed = 12f;
    [SerializeField]
    private float jumpForce = 2f;
    [SerializeField]
    private float lookSens = 3f;
    private float gravity = 9.81f;
    private float landForce = 2f;

    private float jumpDelay = 0.1f;
    private float yLookBound = 90f;

    public bool pickedCharacter = false;
    private GameObject character;

    private Renderer tempBodyRenderer;

    // UI components
    [SerializeField]
    private GameObject playerUI;
    private HealthIndicator healthIndicator;

    Vector3 velocity;
    Vector2 rotation = Vector2.zero;
    Vector2 camRotation = Vector2.zero;
    [SerializeField]
    bool isGrounded;
    float nextJumpTime = 0f;
    float nextSpawnTime = 0f;

    private bool hasRegistered = false;
    private bool doneFirstRespawn = false;

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        HasGameStarted = false;

        IsDead = false;

        playerUI.SetActive(false);

        if (IsOwner)
        {
            controller = GetComponent<CharacterController>();
            cameraGO = GetComponentInChildren<Camera>().gameObject;

            healthIndicator = playerUI.GetComponent<HealthIndicator>();
        }

        if (IsServer)
        {
            Vector3 spawnPosition = Vector3.zero;
            spawnPosition.y += 1.5f;
            gameObject.transform.position = spawnPosition;

            MaxHealth = playerMaxHealth;
            CurrentHealth = MaxHealth;
        }

        SEList = GetComponent<StatusEffectList>();
    }

    void Start()
    {
        if (IsOwner)
        {
            Name = PlayerPrefs.GetString("playerName", "DefaultPlayer");
            if (Name == "")
            {
                Name = "DefaultPlayer";
            }
            SetPlayerStatesServerRpc(Name, Team);
            SceneTransitionHandler.sceneTransitionHandler.OnClientLoadedScene += AlertNewClient;
        }
    }

    void Update()
    {
        switch (SceneTransitionHandler.sceneTransitionHandler.GetCurrentSceneState())
        {
            case SceneTransitionHandler.SceneStates.Ingame:
                {
                    HasGameStarted = true;
                    InGameUpdate();
                    break;
                }
            default:
                {
                    HasGameStarted = false;
                    cameraGO.SetActive(false);
                    break;
                }
        }
    }

    void InGameUpdate()
    {
        if (IsServer)
        {
            if (!hasRegistered)
            {
                if (GameControl.gameControl)
                {
                    GameControl.gameControl.RegisterPlayer(this);
                }

                hasRegistered = true;
            }

            if (IsDead)
            {
                if (Time.time >= nextSpawnTime)
                {
                    // TODO: spawn point logic goes here
                    IsDead = false;
                    CurrentHealth = MaxHealth;
                    RevivePlayerClientRpc();
                    UpdatePlayerHealthClientRpc(CurrentHealth);
                }
            }
        }

        if (IsOwner && !doneFirstRespawn)
        {
            if (GameControl.gameControl)
            {
                Respawn();
                doneFirstRespawn = true;
            }
        }

        if (!IsLocalPlayer || !IsOwner || !HasGameStarted) return;

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        float x = 0;
        float z = 0;
        bool jump = false;

        if (!IsDead)
        {
            if (Input.GetKey(KeyCode.W)) x += 1;
            if (Input.GetKey(KeyCode.S)) x -= 1;
            if (Input.GetKey(KeyCode.D)) z += 1;
            if (Input.GetKey(KeyCode.A)) z -= 1;

            if (Input.GetKeyDown(KeyCode.Space)) jump = true;
        }

        Vector3 move = transform.right * z + transform.forward * x;

        velocity.y -= gravity * Time.deltaTime;

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -landForce;
        }

        if (isGrounded && jump && (Time.time > nextJumpTime))
        {
            velocity.y = 0;
            velocity.y += jumpForce;
            nextJumpTime = Time.time + jumpDelay;
        }

        controller.Move(velocity * Time.deltaTime);

        controller.Move(move * speed * Time.deltaTime);

        if (!IsDead)
        {
            rotation.y += Input.GetAxis("Mouse X");
            camRotation.x += -Input.GetAxis("Mouse Y");
            transform.eulerAngles = (Vector2)rotation * lookSens;

            float newLookBound = yLookBound / lookSens;

            if (camRotation.x < -newLookBound) camRotation.x = -newLookBound;
            if (camRotation.x > newLookBound) camRotation.x = newLookBound;

            cameraGO.transform.localEulerAngles = (Vector2)camRotation * lookSens;
        }
    }

    public void SetCharacter(GameObject newCharacter)
    {
        character = newCharacter;
        pickedCharacter = true;
        tempBodyRenderer = transform.Find("Graphics").GetComponent<Renderer>();
    }

    private void Respawn()
    {
        if (!IsOwner) return;

        IsDead = false;

        Transform spawnPoint = GameControl.gameControl.RequestSpawn(Team);
        Vector3 spawnPosition = spawnPoint.position;
        spawnPosition.y += 1f;

        // yes, this is real.
        controller.enabled = false;
        gameObject.transform.position = spawnPosition;
        gameObject.transform.rotation = spawnPoint.rotation;
        controller.enabled = true;

        cameraGO.SetActive(true);
        playerUI.SetActive(true);
        Cursor.lockState = CursorLockMode.Locked;
    }

    [ClientRpc]
    public void ForcePlayerRespawnClientRpc()
    {
        if (IsOwner)
        {
            // This if fixes a bug caused by the player being told to respawn before their GameControl was singleton'd
            if (GameControl.gameControl)
            {
                Respawn();
                doneFirstRespawn = true;
            }
        }
    }

    [ServerRpc]
    private void SetPlayerStatesServerRpc(string newName, Team newTeam)
    {
        SetPlayerStatesClientRpc(newName, newTeam);
    }

    [ClientRpc]
    public void SetPlayerStatesClientRpc(string newName, Team newTeam)
    {
        if (!IsOwner)
        {
            Name = newName;
            nameplateText.text = Name;
            Team = newTeam;
            // wtf does this line do
            GetComponentInChildren<BillboardCanvas>().targetCamTransform = NetworkManager.LocalClient.PlayerObject.GetComponent<Player>().cameraGO.transform;
            tempBodyRenderer.material.SetColor("_Color", Team == Team.ALPHA ? Color.blue : Color.red);
        }
    }

    [ClientRpc]
    public void InitPlayerClientRpc(string newName, Team newTeam)
    {
        Name = newName;
        Team = newTeam;
    }

    private void AlertNewClient(ulong clientId)
    {
        SetPlayerStatesServerRpc(Name, Team);
    }

    public void TakeDamage(float damageAmount)
    {
        TakeDamageServerRpc(damageAmount);
    }

    [ServerRpc(RequireOwnership = false)]
    public void TakeDamageServerRpc(float damageAmount)
    {
        if (IsDead) return;

        CurrentHealth -= damageAmount;

        if (CurrentHealth <= 0)
        {
            if (!IsDead)
            {
                CurrentHealth = 0f;
                IsDead = true;
                // TODO: MAGIC NUMBER FOR SPAWN DELAY TIME
                nextSpawnTime = Time.time + 5f;
                tempBodyRenderer.material.SetColor("_Color", Color.gray);
            }

            // TODO: somebody got a kill, tell 'em good job
        }

        UpdatePlayerHealthClientRpc(CurrentHealth);
    }

    [ClientRpc]
    public void UpdatePlayerHealthClientRpc(float newHealth)
    {
        if (NetworkManager.IsServer) return;

        CurrentHealth = newHealth;

        if (CurrentHealth <= 0f)
        {
            IsDead = true;
            tempBodyRenderer.material.SetColor("_Color", Color.gray);

            if (IsOwner)
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }

        if (IsOwner)
        {
            healthIndicator.SetDisplay(CurrentHealth);
        }
    }

    [ClientRpc]
    public void RevivePlayerClientRpc()
    {
        IsDead = false;
        tempBodyRenderer.material.SetColor("_Color", Team == Team.ALPHA ? Color.blue : Color.red);

        if (IsOwner)
        {
            Respawn();
        }
    }

    public Team GetTeam()
    {
        return Team;
    }

}