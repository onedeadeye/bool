using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionStatusEffect : MonoBehaviour
{
    [SerializeField]
    private float radius = 1f;

    [SerializeField]
    private int statusEffect = 1;

    // live variables
    private bool isDummy = false;

    private Team team;

    private ProjectileResult governor;

    // Start is called before the first frame update
    void Start()
    {
        governor = GetComponent<ProjectileResult>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!governor.isSet)
        {
            return;
        }

        isDummy = governor.isDummy;

        team = governor.team;

        if (!isDummy)
        {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);

            foreach (Collider hitCollider in hitColliders)
            {
                if (hitCollider.CompareTag("Player") || hitCollider.CompareTag("Enemy"))
                {
                    Team hitTeam = hitCollider.GetComponent<ICharacter>().GetTeam();
                    if (hitTeam != team && hitTeam != Team.NONE)
                    {
                        hitCollider.GetComponent<ICharacter>().SEList.AddStatusEffectServerRpc(statusEffect);
                    }
                }
            }
        }

        this.enabled = false;
    }
}
