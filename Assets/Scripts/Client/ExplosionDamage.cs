using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionDamage : MonoBehaviour
{
    [SerializeField]
    private float radius = 1f;

    [SerializeField]
    private float centerDamage = 1f;

    [SerializeField]
    private float edgeDamage = 1f;

    // live variables
    private bool isDummy = false;

    private Team team;

    private ProjectileResult governor;

    // Start is called before the first frame update
    void Start()
    {
        governor = GetComponent<ProjectileResult>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!governor.isSet)
        {
            return;
        }

        isDummy = governor.isDummy;

        team = governor.team;

        if (!isDummy)
        {
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, radius);

            foreach (Collider hitCollider in hitColliders)
            {
                if (hitCollider.CompareTag("Player") || hitCollider.CompareTag("Enemy"))
                {
                    Team hitTeam = hitCollider.GetComponent<ICharacter>().GetTeam();
                    if (hitTeam != team && hitTeam != Team.NONE)
                    {
                        float distance = Vector3.Distance(transform.position, hitCollider.transform.position);
                        float percentRadius = distance / radius;
                        float damageToDeal = Mathf.Lerp(centerDamage, edgeDamage, percentRadius);
                        hitCollider.GetComponent<ICharacter>().TakeDamage(damageToDeal);
                    }
                }
            }
        }

        this.enabled = false;
    }
}
