using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class CharClassHost : NetworkBehaviour
{
    static public CharClassHost charClassHost { get; internal set; }

    [SerializeField]
    public GameObject[] characterClasses;

    [SerializeField]
    private GameObject canvas;

    private int currentPreview = 0;

    // Singleton implementation
    void Awake()
    {
        if (charClassHost != this && charClassHost != null)
        {
            GameObject.Destroy(charClassHost.gameObject);
        }
        charClassHost = this;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PreviewCharacter(int index)
    {
        currentPreview = index;
    }

    public void SelectCharacter()
    {
        GameObject newCharacter = Instantiate(characterClasses[currentPreview]);
        Player localPlayer = NetworkManager.LocalClient.PlayerObject.GetComponent<Player>();
        newCharacter.transform.SetParent(localPlayer.transform);
        localPlayer.SetCharacter(newCharacter);
        SpawnCharacterServerRpc(currentPreview, NetworkManager.LocalClientId);
        canvas.SetActive(false);
    }

    [ServerRpc]
    private void SpawnCharacterServerRpc(int index, ulong clientId)
    {
        SpawnCharacterClientRpc(index, clientId);
    }

    [ClientRpc]
    private void SpawnCharacterClientRpc(int index, ulong clientId)
    {
        if (clientId == NetworkManager.LocalClientId) return;

        GameObject newCharacter = Instantiate(characterClasses[currentPreview]);
        Player remotePlayer = NetworkManager.ConnectedClients[clientId].PlayerObject.GetComponent<Player>();
        newCharacter.transform.SetParent(remotePlayer.transform);
        remotePlayer.SetCharacter(newCharacter);
    }
}