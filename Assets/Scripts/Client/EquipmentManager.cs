using UnityEngine;
using Unity.Netcode;

public class EquipmentManager : NetworkBehaviour
{
    private GameObject primaryWeaponHost;
    private GameObject secondaryWeaponHost;
    private GameObject meleeWeaponHost;

    private int primaryIndex = 0;
    private int secondaryIndex = 0;

    private GameObject primaryWeapon;
    private GameObject secondaryWeapon;
    private GameObject meleeWeapon;

    private bool weaponsLocked = false;

    public enum WeaponSlot
    {
        PRIMARY,
        SECONDARY,
        MELEE,
        EQUIPMENT,
        UNSET
    }

    private WeaponSlot activeWeapon = WeaponSlot.UNSET;

    // Start is called before the first frame update
    void Awake()
    {
        primaryWeaponHost = transform.GetChild(0).gameObject;
        secondaryWeaponHost = transform.GetChild(1).gameObject;
        meleeWeaponHost = transform.GetChild(2).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsLocalPlayer) return;

        if (!weaponsLocked)
        {
            Debug.Log("Haven't received our weapons yet!");
            return;
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SwitchWeapon(WeaponSlot.PRIMARY);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SwitchWeapon(WeaponSlot.SECONDARY);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SwitchWeapon(WeaponSlot.MELEE);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            SwitchWeapon(WeaponSlot.EQUIPMENT);
        }
    }

    public void SelectAlternateWeapon(int slot, int index)
    {
        switch (slot)
        {
            case 0:
                primaryIndex = index;
                break;
            case 1:
                secondaryIndex = index;
                break;
            case 2:
                // melees never change
                break;
            case 3:
                // equipment
                break;
            default:
                break;
        }
    }

    public void LockWeapons()
    {
        primaryWeapon = primaryWeaponHost.transform.GetChild(primaryIndex).gameObject;
        secondaryWeapon = secondaryWeaponHost.transform.GetChild(secondaryIndex).gameObject;
        meleeWeapon = meleeWeaponHost.transform.GetChild(0).gameObject;

        weaponsLocked = true;

        SwitchWeapon(WeaponSlot.PRIMARY);
    }

    [ServerRpc]
    public void LockWeaponsServerRpc(int p, int s)
    {
        LockWeaponsClientRpc(p, s);
    }

    [ClientRpc]
    public void LockWeaponsClientRpc(int p, int s)
    {
        if (IsOwner) return;

        primaryWeapon = primaryWeaponHost.transform.GetChild(p).gameObject;
        secondaryWeapon = secondaryWeaponHost.transform.GetChild(s).gameObject;
        meleeWeapon = meleeWeaponHost.transform.GetChild(0).gameObject;

        weaponsLocked = true;
        SwitchWeapon(WeaponSlot.PRIMARY);
    }

    private GameObject GetWeapon(WeaponSlot aW)
    {
        switch (aW)
        {
            case WeaponSlot.PRIMARY:
                return primaryWeapon;
            case WeaponSlot.SECONDARY:
                return secondaryWeapon;
            case WeaponSlot.MELEE:
                return meleeWeapon;
            default:
                return primaryWeapon;
        }
    }

    private void DequipAllWeapons()
    {
        primaryWeapon.gameObject.SetActive(false);
        secondaryWeapon.gameObject.SetActive(false);
        meleeWeapon.gameObject.SetActive(false);
    }

    private void SwitchWeapon(WeaponSlot switchTo)
    {
        if (switchTo == activeWeapon) return;

        if (IsLocalPlayer)
        {
            SwitchWeaponServerRpc(EncodeWeaponSlot(switchTo));
        }

        DequipAllWeapons();

        GetWeapon(switchTo).SetActive(true);
        activeWeapon = switchTo;
    }

    [ServerRpc]
    private void SwitchWeaponServerRpc(int slotInt)
    {
        SwitchWeaponClientRpc(slotInt);
    }

    [ClientRpc]
    private void SwitchWeaponClientRpc(int slotInt)
    {
        if (!IsLocalPlayer)
        {
            SwitchWeapon(DecodeWeaponSlot(slotInt));
        }
    }

    private int EncodeWeaponSlot(WeaponSlot slot)
    {
        switch (slot)
        {
            case WeaponSlot.PRIMARY:
                return 1;
            case WeaponSlot.SECONDARY:
                return 2;
            case WeaponSlot.MELEE:
                return 3;
            case WeaponSlot.EQUIPMENT:
                return 4;
            default:
                return 1;
        }
    }

    private WeaponSlot DecodeWeaponSlot(int slotInt)
    {
        switch (slotInt)
        {
            case 1:
                return WeaponSlot.PRIMARY;
            case 2:
                return WeaponSlot.SECONDARY;
            case 3:
                return WeaponSlot.MELEE;
            case 4:
                return WeaponSlot.EQUIPMENT;
            default:
                return WeaponSlot.PRIMARY;
        }
    }
}
