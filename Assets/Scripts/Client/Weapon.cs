using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class Weapon : NetworkBehaviour
{
    private bool realWeapon = false;

    // Scarcity of ammo drops
    public enum AmmoType
    {
        PROLIFIC,
        AVAILABLE,
        EGREGIOUS
    }

    [Header("Weapon Settings")]
    public string weaponName = "DefaultWeapon";
    public bool isHitscan = true;
    public int roundsPerMinute = 150;
    public bool fullAuto = false;
    [ConditionalHide("isHitscan")]
    public int hitDamage = 10;
    [ConditionalHide("isHitscan")]
    public float hitRange = 20f;

    [Header("Trail Settings")]
    [ConditionalHide("isHitscan")]
    public bool drawTrail = false;
    [ConditionalHide("isHitscan")]
    public Color trailColor;
    [ConditionalHide("isHitscan")]
    public float trailDuration = 0.2f;
    public GameObject muzzlePoint;

    [Header("Effect Settings")]
    public GameObject effectPrefab;
    [ConditionalHide("isHitscan")]
    public float impactDuration = 0.2f;
    // 0 indicates no effect
    public int statusEffect = 0;

    [Header("Ammo Settings")]
    public AmmoType ammoType = AmmoType.PROLIFIC;
    public int maxReserveAmmo = 100;

    [SerializeField]
    private GameObject cameraGO;

    // Live variables
    private float nextTimeToFire = 0f;
    private int currentReserveAmmo = 100;

    private Player player;
    private Team team;

    // Start is called before the first frame update
    void Start()
    {
        if (!realWeapon) return;

        if (IsLocalPlayer)
        {
            InitializeStats();
        }

        player = transform.parent.parent.GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (team == Team.NONE)
        {
            team = player.Team;
        }

        if (!realWeapon) return;

        // This whole thing only runs on the side of the owner
        if (!IsLocalPlayer) return;

        if (player.IsDead) return;

        switch (SceneTransitionHandler.sceneTransitionHandler.GetCurrentSceneState())
        {
            case SceneTransitionHandler.SceneStates.Ingame:
                {
                    break;
                }
            default:
                {
                    return;
                }
        }

        Vector3 newPosition = cameraGO.transform.position + (cameraGO.transform.up.normalized * -0.5f);
        transform.position = newPosition;
        transform.rotation = cameraGO.transform.rotation;

        // If we're trying to shoot (switch based on fullAuto) and we're at or past our nextTimeToFire
        if (fullAuto ? Input.GetMouseButton(0) : Input.GetMouseButtonDown(0))
        {
            if (Time.time >= nextTimeToFire)
            {
                // If we have ammo
                if (currentReserveAmmo > 0)
                {
                    currentReserveAmmo--;

                    if (isHitscan)
                    {
                        Ray ray = new Ray(cameraGO.transform.position, transform.forward);
                        RaycastHit hit;
                        Physics.Raycast(ray, out hit, hitRange);
                        if (hit.collider)
                        {
                            if (hit.collider.CompareTag("Player") || hit.collider.CompareTag("Enemy"))
                            {
                                Team hitTeam = hit.collider.GetComponent<ICharacter>().GetTeam();
                                if (hitTeam != team && hitTeam != Team.NONE)
                                {
                                    ICharacter character = hit.transform.GetComponent<ICharacter>();
                                    character.TakeDamage(hitDamage);
                                    if (statusEffect != 0)
                                    {
                                        character.SEList.AddStatusEffectServerRpc(statusEffect);
                                    }
                                }
                            }
                        }


                        if (drawTrail)
                        {
                            if (hit.collider)
                            {
                                DrawBulletTrail(hit.point);
                            }
                            else
                            {
                                DrawBulletTrail(ray.origin + ray.direction * hitRange);
                            }
                        }

                        if (effectPrefab && hit.collider)
                        {
                            CreateImpactVFX(hit.point, hit.normal);
                        }
                    }
                    else
                    {
                        CreateProjectileObject();
                    }
                    nextTimeToFire = Time.time + (60f / roundsPerMinute);
                }
                else
                {
                    // No ammo, do a little "click"
                }
            }
        }
    }

    public void InitializeStats()
    {
        currentReserveAmmo = maxReserveAmmo;
        realWeapon = true;
    }

    [ServerRpc]
    private void DrawBulletTrailServerRpc(Vector3 end)
    {
        DrawBulletTrailClientRpc(end);
    }

    [ClientRpc]
    private void DrawBulletTrailClientRpc(Vector3 end)
    {
        // Local player does their own, instant version of this.
        if (!IsLocalPlayer)
        {
            DrawBulletTrail(end);
        }
    }

    public void DrawBulletTrail(Vector3 end)
    {
        if (IsLocalPlayer)
        {
            DrawBulletTrailServerRpc(end);
        }

        GameObject bulletTrail = new GameObject();
        bulletTrail.transform.position = muzzlePoint.transform.position;
        LineRenderer lr = bulletTrail.AddComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Sprites/Default"));
        lr.startColor = trailColor;
        lr.endColor = trailColor;
        lr.startWidth = 0.1f;
        lr.endWidth = 0.1f;
        lr.SetPosition(0, muzzlePoint.transform.position);
        lr.SetPosition(1, end);
        Destroy(bulletTrail, trailDuration);
    }

    [ServerRpc]
    private void CreateImpactVFXServerRpc(Vector3 point, Vector3 hitDirection)
    {
        CreateImpactVFXClientRpc(point, hitDirection);
    }

    [ClientRpc]
    private void CreateImpactVFXClientRpc(Vector3 point, Vector3 hitDirection)
    {
        // Local player does their own, instant version of this.
        if (!IsLocalPlayer)
        {
            CreateImpactVFX(point, hitDirection);
        }
    }

    private void CreateImpactVFX(Vector3 point, Vector3 hitDirection)
    {
        if (IsLocalPlayer)
        {
            CreateImpactVFXServerRpc(point, hitDirection);
        }

        GameObject vfx = Instantiate(effectPrefab);
        vfx.transform.position = point;
        vfx.transform.forward = -hitDirection;
        Destroy(vfx, impactDuration);
    }

    [ServerRpc]
    private void CreateProjectileObjectServerRpc()
    {
        CreateProjectileObjectClientRpc();
    }

    [ClientRpc]
    private void CreateProjectileObjectClientRpc()
    {
        if (!IsLocalPlayer)
        {
            CreateProjectileObject();
        }
    }

    private void CreateProjectileObject()
    {
        if (IsLocalPlayer)
        {
            CreateProjectileObjectServerRpc();
        }

        GameObject newProjectileGO = Instantiate(effectPrefab);
        Projectile newProjectile = newProjectileGO.GetComponent<Projectile>();

        newProjectileGO.transform.position = cameraGO.transform.position;
        newProjectileGO.transform.forward = cameraGO.transform.forward;

        newProjectile.isDummy = !IsLocalPlayer;
        // this will surely break. hello future me
        newProjectile.Team = team;
        newProjectile.active = true;
    }
}
