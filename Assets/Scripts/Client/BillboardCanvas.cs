using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardCanvas : MonoBehaviour
{
    public Transform targetCamTransform;

    private Quaternion originalRotation;

    // Start is called before the first frame update
    void Start()
    {
        originalRotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (targetCamTransform)
        {
            transform.rotation = targetCamTransform.rotation * originalRotation;
            transform.Rotate(Vector3.up, 180f);
        }
    }
}
