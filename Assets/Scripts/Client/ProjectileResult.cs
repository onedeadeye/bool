using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileResult : MonoBehaviour
{
    // live variables
    [HideInInspector]
    public bool isDummy = false;

    [HideInInspector]
    public Team team;

    [HideInInspector]
    public bool isSet = false;

    [SerializeField]
    private float lifetime = 0f;

    void Start()
    {
        if (lifetime > 0f)
        {
            Destroy(gameObject, lifetime);
        }
    }

}
