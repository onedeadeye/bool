using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthIndicator : MonoBehaviour
{
    [SerializeField]
    private RectTransform healthImage;
    [SerializeField]
    private Text healthText;

    private RectTransform rectT;

    void Start()
    {
        rectT = healthImage.GetComponent<RectTransform>();
    }

    public void SetDisplay(float health)
    {
        rectT.sizeDelta = new Vector2(health * 3, 50);
        healthText.text = "" + ((int)health);
    }
}
