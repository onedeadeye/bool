using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class GameControl : NetworkBehaviour
{
    static public GameControl gameControl { get; internal set; }

    [SerializeField]
    private Transform[] AlphaSpawns;
    [SerializeField]
    private Transform[] BetaSpawns;
    [SerializeField]
    private Transform defaultSpawn;

    private List<Player> players = new List<Player> { };

    // Singleton implementation
    void Awake()
    {
        if (gameControl != this && gameControl != null)
        {
            GameObject.Destroy(gameControl.gameObject);
        }
        gameControl = this;

        SceneTransitionHandler.sceneTransitionHandler.SetSceneState(SceneTransitionHandler.SceneStates.Ingame);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void RegisterPlayer(Player player)
    {
        players.Add(player);
        player.ForcePlayerRespawnClientRpc();
    }

    public Transform RequestSpawn(Team team)
    {
        switch (team)
        {
            case Team.ALPHA:
                {
                    return AlphaSpawns[0];
                }
            case Team.BETA:
                {
                    return BetaSpawns[0];
                }
            default:
                {
                    return defaultSpawn.transform;
                }
        }
    }
}
