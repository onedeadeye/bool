using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnFireSE : StatusEffect
{
    public override string Name { get; set; } = "On Fire";
    // both of these are in seconds
    public override float Duration { get; set; } = 2f;
    public override float TickRate { get; set; } = 0.5f;
    public override bool IsExclusive { get; set; } = true;

    public override void TickSE(ICharacter character)
    {
        character.TakeDamage(damage);
    }

    private float damage = 5f;
}
