using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class StatusEffect
{
    // abstracts start here
    public abstract string Name { get; set; }
    // both of these are in seconds
    public abstract float Duration { get; set; }
    public abstract float TickRate { get; set; }
    public abstract bool IsExclusive { get; set; }

    public abstract void TickSE(ICharacter character);
    // abstracts end here

    public float ExpireTime = 0f;
    public float NextTickTime = 0f;
}
