using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class StatusEffectList : NetworkBehaviour
{
    private List<StatusEffect> statusEffects = new List<StatusEffect>();
    private List<StatusEffect> effectsToClear = new List<StatusEffect>();

    private ICharacter character;

    // Start is called before the first frame update
    void Start()
    {
        character = GetComponent<ICharacter>();
    }

    // Update is called once per frame
    void Update()
    {
        effectsToClear.Clear();

        foreach (StatusEffect effect in statusEffects)
        {
            if (IsServer)
            {
                if (Time.time >= effect.NextTickTime)
                {
                    effect.TickSE(character);
                    effect.NextTickTime = Time.time + effect.TickRate;
                }
            }

            // this is where VFX code would go

            if (Time.time >= effect.ExpireTime) effectsToClear.Add(effect);
        }

        foreach (StatusEffect effect in effectsToClear)
        {
            statusEffects.Remove(effect);
        }
    }

    // Given an int, uses the lookup table to produce a StatusEffect on the server.
    // An additional ClientRPC is called to synchronize clients if applicable
    [ServerRpc(RequireOwnership = false)]
    public void AddStatusEffectServerRpc(int status)
    {
        StatusEffect newEffect = StatusLookup(status);

        // Exclusive effects cannot "stack". If we already have one, don't add another.
        if (newEffect.IsExclusive)
        {
            foreach (StatusEffect effect in statusEffects)
            {
                if (effect.GetType() == newEffect.GetType()) return;
            }
        }

        statusEffects.Add(newEffect);
        AddStatusEffectClientRpc(status);
    }

    [ClientRpc]
    private void AddStatusEffectClientRpc(int status)
    {
        statusEffects.Add(StatusLookup(status));
    }

    // Translates an integer to a StatusEffect object. Index 0 is used for "no effect" in code.
    private StatusEffect StatusLookup(int status)
    {
        StatusEffect newEffect;
        switch (status)
        {
            case 1: newEffect = new OnFireSE(); break;
            default: newEffect = new DefaultSE(); break;
        }
        newEffect.ExpireTime = Time.time + newEffect.Duration;
        return newEffect;
    }
}
