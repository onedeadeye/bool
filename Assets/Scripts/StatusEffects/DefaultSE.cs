using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefaultSE : StatusEffect
{
    public override string Name { get; set; } = "DefaultSE";
    // both of these are in seconds
    public override float Duration { get; set; } = 1f;
    public override float TickRate { get; set; } = 1f;
    public override bool IsExclusive { get; set; } = true;

    public override void TickSE(ICharacter character)
    {
        Debug.Log("this is a default status effect and should never be created");
    }
}
