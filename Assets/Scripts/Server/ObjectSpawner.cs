using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class ObjectSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject spawnedObject;

    [SerializeField]
    float spawnDelay = 5f;

    float nextSpawnTime = 0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time >= nextSpawnTime)
        {
            Spawn();
            nextSpawnTime = Time.time + spawnDelay;
        }
    }

    public void Spawn()
    {
        GameObject go = Instantiate(spawnedObject, transform.position, Quaternion.identity);
        go.GetComponent<NetworkObject>().Spawn();
    }
}
