using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class ServerObject : NetworkBehaviour
{
    void Awake()
    {
        if (!NetworkManager.Singleton.IsServer)
        {
            Destroy(gameObject);
        }
    }
}
