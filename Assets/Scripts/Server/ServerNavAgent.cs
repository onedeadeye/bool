using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Unity.Netcode;

public class ServerNavAgent : NetworkBehaviour, ICharacter
{
    // ICharacter implementation
    public string Name { get; set; }
    public int MaxHealth { get; set; }
    public float CurrentHealth { get; set; }
    public bool IsDead { get; set; }
    public Team team = Team.NONE;
    public StatusEffectList SEList { get; set; }

    [SerializeField]
    private string enemyName;
    [SerializeField]
    private int enemyMaxHealth;

    private NavMeshAgent agent;

    private GameObject[] potentialTargets;

    [SerializeField]
    private float searchDelay = 0.5f;
    private float nextSearchTime;

    // Start is called before the first frame update
    void Start()
    {
        Name = enemyName;
        MaxHealth = enemyMaxHealth;
        agent = GetComponent<NavMeshAgent>();
        SEList = GetComponent<StatusEffectList>();

        if (IsServer)
        {
            potentialTargets = GameObject.FindGameObjectsWithTag("Player");
            // Whenever we set a nextsearchtime we give it an offset to mitigate lag spikes
            nextSearchTime = Time.time + searchDelay + Random.Range(0f, 0.25f);
            CurrentHealth = MaxHealth;
        }
        else
        {
            agent.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!NetworkManager.IsServer)
        {
            return;
        }

        if (CurrentHealth <= 0f && !IsDead)
        {
            CurrentHealth = 0f;
            IsDead = true;
            agent.enabled = false;
            GetComponent<Renderer>().material.SetColor("_Color", Color.gray);
            Destroy(gameObject, 2f);
            KillAgentClientRpc();
            return;
        }

        if (IsDead)
        {
            return;
        }

        if (Time.time >= nextSearchTime)
        {
            // modify this to be a maximum search radius
            float closestDistance = 1000f;
            foreach (GameObject target in potentialTargets)
            {
                float newDistance = Vector3.Distance(this.transform.position, target.transform.position);
                if (newDistance < closestDistance)
                {
                    closestDistance = newDistance;
                    agent.destination = target.transform.position;
                }
            }
            nextSearchTime = Time.time + searchDelay + Random.Range(0f, 0.25f);
        }
    }

    public void TakeDamage(float damageAmount)
    {
        DamageServerRpc(damageAmount);
    }

    [ServerRpc(RequireOwnership = false)]
    private void DamageServerRpc(float damageAmount)
    {
        if (!IsDead)
        {
            CurrentHealth -= damageAmount;
        }
    }

    [ClientRpc]
    private void KillAgentClientRpc()
    {
        if (NetworkManager.IsServer)
        {
            return;
        }
        IsDead = true;
        GetComponent<Renderer>().material.SetColor("_Color", Color.gray);
    }

    public Team GetTeam()
    {
        return team;
    }
}
