﻿using System;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class LobbyControl : NetworkBehaviour
{
    [HideInInspector]
    public static bool isHosting;

    [SerializeField]
    private string m_InGameSceneName = "InGame";

    // Minimum player count required to transition to next level
    [SerializeField]
    private int m_MinimumPlayerCount = 1;

    public Text LobbyText;
    private bool m_AllPlayersInLobby;

    private Dictionary<ulong, bool> m_ClientsInLobbyLoaded;
    private List<ulong> AlphaTeam = new List<ulong>();
    private List<ulong> BetaTeam = new List<ulong>();
    private string m_UserLobbyStatusText;

    private float nextUpdateTime = 0f;

    /// <summary>
    ///     Awake
    ///     This is one way to kick off a multiplayer session
    /// </summary>
    private void Awake()
    {
        m_ClientsInLobbyLoaded = new Dictionary<ulong, bool>();

        //We added this information to tell us if we are going to host a game or join an the game session
        if (isHosting)
            NetworkManager.Singleton.StartHost(); //Spin up the host
        else
            NetworkManager.Singleton.StartClient(); //Spin up the client

        if (NetworkManager.Singleton.IsListening)
        {
            //Always add ourselves to the list at first
            m_ClientsInLobbyLoaded.Add(NetworkManager.Singleton.LocalClientId, false);

            //If we are hosting, then handle the server side for detecting when clients have connected
            //and when their lobby scenes are finished loading.
            if (IsServer)
            {
                m_AllPlayersInLobby = false;

                //Server will be notified when a client connects
                NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnectedCallback;
                SceneTransitionHandler.sceneTransitionHandler.OnClientLoadedScene += ClientLoadedScene;
            }

            //Update our lobby
            GenerateUserStatsForLobby();
        }

        SceneTransitionHandler.sceneTransitionHandler.SetSceneState(SceneTransitionHandler.SceneStates.Lobby);
    }

    void FixedUpdate()
    {
        if (Time.time >= nextUpdateTime)
        {
            GenerateUserStatsForLobby();
            nextUpdateTime = Time.time + 1f;
        }
    }

    private void OnGUI()
    {
        if (LobbyText != null) LobbyText.text = m_UserLobbyStatusText;
    }

    /// <summary>
    ///     GenerateUserStatsForLobby
    ///     Psuedo code for setting player state
    ///     Just updating a text field, this could use a lot of "refactoring"  :)
    /// </summary>
    private void GenerateUserStatsForLobby()
    {
        //Debug.Log("Generating player list");
        m_UserLobbyStatusText = string.Empty;
        GameObject[] loadedPlayers = GameObject.FindGameObjectsWithTag("Player");

        foreach (var clientLobbyStatus in m_ClientsInLobbyLoaded)
        {
            //Debug.Log("Client id " + clientLobbyStatus.Key + ", finding gameobject...");
            string targetsName = "DefaultPlayer";

            foreach (GameObject player in loadedPlayers)
            {
                //Debug.Log("Player '" + player.GetComponent<Player>().Name + "', owner " + player.GetComponent<NetworkObject>().OwnerClientId);
                if (player.GetComponent<NetworkObject>().OwnerClientId == clientLobbyStatus.Key)
                {
                    Player playerScript = player.GetComponent<Player>();
                    targetsName = playerScript.Name;

                    if (IsServer)
                    {
                        playerScript.SetPlayerStatesClientRpc(playerScript.Name, playerScript.Team);
                    }

                    goto PlayerFound;
                }
            }

            // If the goto doesn't trip, meaning we found a linked gameObject, it's probably that phantom UID
            // that I assume is created by the local client with some odd number
            continue;

        PlayerFound:
            m_UserLobbyStatusText += targetsName + " | ";

            m_UserLobbyStatusText += clientLobbyStatus.Value ? "(Ready)\n" : "(Not Ready)\n";
        }
    }

    /// <summary>
    ///     UpdateAndCheckPlayersInLobby
    ///     Checks to see if we have enough people to start
    /// </summary>
    private void UpdateAndCheckPlayersInLobby()
    {
        m_AllPlayersInLobby = m_ClientsInLobbyLoaded.Count >= m_MinimumPlayerCount;

        foreach (var clientLobbyStatus in m_ClientsInLobbyLoaded)
        {
            SendClientReadyStatusUpdatesClientRpc(clientLobbyStatus.Key, clientLobbyStatus.Value);
            if (!NetworkManager.Singleton.ConnectedClients.ContainsKey(clientLobbyStatus.Key))

                //If some clients are still loading into the lobby scene then this is false
                m_AllPlayersInLobby = false;
        }

        CheckForAllPlayersReady();
    }

    /// <summary>
    ///     ClientLoadedScene
    ///     Invoked when a client has loaded this scene
    /// </summary>
    /// <param name="clientId"></param>
    private void ClientLoadedScene(ulong clientId)
    {
        if (IsServer)
        {
            if (!m_ClientsInLobbyLoaded.ContainsKey(clientId))
            {
                m_ClientsInLobbyLoaded.Add(clientId, false);

                GenerateUserStatsForLobby();
            }

            UpdateAndCheckPlayersInLobby();
        }
    }

    /// <summary>
    ///     OnClientConnectedCallback
    ///     Since we are entering a lobby and MLAPI NetowrkingManager is spawning the player,
    ///     the server can be configured to only listen for connected clients at this stage.
    /// </summary>
    /// <param name="clientId">client that connected</param>
    private void OnClientConnectedCallback(ulong clientId)
    {
        if (IsServer)
        {
            if (!m_ClientsInLobbyLoaded.ContainsKey(clientId)) m_ClientsInLobbyLoaded.Add(clientId, false);
            GenerateUserStatsForLobby();

            UpdateAndCheckPlayersInLobby();
        }
    }

    /// <summary>
    ///     SendClientReadyStatusUpdatesClientRpc
    ///     Sent from the server to the client when a player's status is updated.
    ///     This also populates the connected clients' (excluding host) player state in the lobby
    /// </summary>
    /// <param name="clientId"></param>
    /// <param name="isReady"></param>
    [ClientRpc]
    private void SendClientReadyStatusUpdatesClientRpc(ulong clientId, bool isReady)
    {
        if (!IsServer)
        {
            if (!m_ClientsInLobbyLoaded.ContainsKey(clientId))
                m_ClientsInLobbyLoaded.Add(clientId, isReady);
            else
                m_ClientsInLobbyLoaded[clientId] = isReady;
            GenerateUserStatsForLobby();
        }
    }

    /// <summary>
    ///     CheckForAllPlayersReady
    ///     Checks to see if all players are ready, and if so launches the game
    /// </summary>
    private void CheckForAllPlayersReady()
    {
        if (m_AllPlayersInLobby)
        {
            var allPlayersAreReady = true;
            foreach (var clientLobbyStatus in m_ClientsInLobbyLoaded)
                if (!clientLobbyStatus.Value)

                    //If some clients are still loading into the lobby scene then this is false
                    allPlayersAreReady = false;

            //Only if all players are ready
            if (allPlayersAreReady)
            {
                //Remove our client connected callback
                NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnectedCallback;

                //Remove our scene loaded callback
                SceneTransitionHandler.sceneTransitionHandler.OnClientLoadedScene -= ClientLoadedScene;

                // Set the team of each of our players
                GameObject[] loadedPlayers = GameObject.FindGameObjectsWithTag("Player");
                foreach (var clientLobbyStatus in m_ClientsInLobbyLoaded)
                {
                    foreach (GameObject player in loadedPlayers)
                    {
                        if (player.GetComponent<NetworkObject>().OwnerClientId == clientLobbyStatus.Key)
                        {
                            Team joinTeam = Team.NONE;
                            if (AlphaTeam.Count <= BetaTeam.Count)
                            {
                                joinTeam = Team.ALPHA;
                                AlphaTeam.Add(OwnerClientId);
                            }
                            else
                            {
                                joinTeam = Team.BETA;
                                BetaTeam.Add(OwnerClientId);
                            }
                            string playerName = player.GetComponent<Player>().Name;
                            player.GetComponent<Player>().InitPlayerClientRpc(playerName, joinTeam);
                        }
                    }
                }

                //Transition to the ingame scene
                SceneTransitionHandler.sceneTransitionHandler.SwitchScene(m_InGameSceneName);
            }
        }
    }

    /// <summary>
    ///     PlayerIsReady
    ///     Tied to the Ready button in the lobby scene
    /// </summary>
    public void PlayerIsReady()
    {
        if (IsServer)
        {
            m_ClientsInLobbyLoaded[NetworkManager.Singleton.ServerClientId] = true;
            UpdateAndCheckPlayersInLobby();
        }
        else
        {
            m_ClientsInLobbyLoaded[NetworkManager.Singleton.LocalClientId] = true;
            OnClientIsReadyServerRpc(NetworkManager.Singleton.LocalClientId);
        }

        GenerateUserStatsForLobby();
    }

    /// <summary>
    ///     OnClientIsReadyServerRpc
    ///     Sent to the server when the player clicks the ready button
    /// </summary>
    /// <param name="clientid">clientId that is ready</param>
    [ServerRpc(RequireOwnership = false)]
    private void OnClientIsReadyServerRpc(ulong clientid)
    {
        if (m_ClientsInLobbyLoaded.ContainsKey(clientid))
        {
            m_ClientsInLobbyLoaded[clientid] = true;
            UpdateAndCheckPlayersInLobby();
            GenerateUserStatsForLobby();
        }
    }
}