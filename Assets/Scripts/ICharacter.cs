public enum Team
{
    NONE,
    ALPHA,
    BETA
}

public interface ICharacter
{
    public string Name { get; set; }
    public int MaxHealth { get; set; }
    public float CurrentHealth { get; set; }
    public bool IsDead { get; set; }
    public StatusEffectList SEList { get; set; }

    void TakeDamage(float damageAmount);

    Team GetTeam();
}